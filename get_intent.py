import pickle
import os
import tensorflow as tf
import tflearn
import numpy as np
from nltk.tokenize import word_tokenize
from nltk.stem.lancaster import LancasterStemmer
from nltk.tag.stanford import StanfordNERTagger

import json


class Generate_intent:

    #gets the ner model and initializes to the variable
    def __init__(self,tolerance,ner_model,jarfile):
        self.directory=os.getcwd()
        self.stemmer=LancasterStemmer()
        self.tolerance=tolerance
        self.ner_tagger=StanfordNERTagger(ner_model,jarfile)

    #loads the pickle files
    def load_meta_data(self):
        data=pickle.load(open(self.directory + '/pickles/t_metadata','rb'))
        self.tokens=data['tokens']
        self.labels=data['labels']

        self.train_x=data['train_x']
        self.train_y=data['train_y']

        print(self.train_x)
        print (self.train_y)
        return ("<-----@loaded_pickles------->")

    def prepare_model(self):
        input_layer = tflearn.input_data(shape=[None, len(self.train_x[0])])
        hidden_layer1 = tflearn.fully_connected(input_layer, 8)
        hidden_layer2 = tflearn.fully_connected(hidden_layer1, 8)
        output_layer = tflearn.fully_connected(hidden_layer2, len(self.train_y[0]), activation='softmax')
        regression_model = tflearn.regression(output_layer)
        self.model = tflearn.DNN(regression_model, tensorboard_dir='tflearn_logs')
        self.model.load(self.directory+'/preprocessingcore/R_models/intent_classifier.tflearn')
        return ("<-----@model_loaded_sucessfully-------> ")

    def preprocess(self,sentence):
        tokens=word_tokenize(sentence)
        self.temp_tokens=tokens
        tokens=[self.stemmer.stem(word.lower())for word in tokens]
        return tokens

    def word_container(self,sentence, words):
        tokens = self.preprocess(sentence)
        cbow = [0] * len(words)
        for word in tokens:
            for i, w in enumerate(words):
                if w == word:
                    cbow[i] = 1
        return (np.array(cbow))


    #gets the intent of the input sentence
    def intent_identifier(self,sentence):
        # generate confidence of the classification
        results = self.model.predict([self.word_container(sentence, self.tokens)])[0]
        # only include the items with higher confidence
        results = [[i, r] for i, r in enumerate(results) if r > self.tolerance]
        # sort by strength of probability
        results.sort(key=lambda x: x[1], reverse=True)
        return_list = []
        for r in results:
            return_list.append((self.labels[r[0]], r[1]))
            # return tuple of intent and probability
        return return_list

    #implements stanford ner to identify the intent
    def entity_extractor(self):
        entities = []
        entities_tracker={}
        temp_entities=[]
        tagged_tuples = self.ner_tagger.tag(self.temp_tokens)
        previous_tag = ''
        for token,tag in tagged_tuples:

            if tag != 'O' and (previous_tag == [] or tag in previous_tag):
                temp_entities.append(token)
                previous_tag = tag
            elif tag != 'O' and tag not in previous_tag:
                entity=(' '.join(temp_entities))
                entities.append(entity)
                if previous_tag !='':
                    entities_tracker.update({previous_tag:entity})
                else:
                    pass
                temp_entities = []
                temp_entities.append(token)
                previous_tag = tag
        if temp_entities:
            entities.append(' '.join(temp_entities))
        if previous_tag != '':
            entities_tracker.update({previous_tag:' '.join(temp_entities)})
        else:
            pass
        return entities_tracker,entities














