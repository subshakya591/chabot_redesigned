import os
import json
from py2neo import Graph, Node, Relationship

from dialougeflow import dialouges


class CreateGraphKB:

    def __init__(self):
        self.graph = Graph('bolt://localhost:7687', auth=('neo4j', 'subodh'))

    def word_normalizer(self,name):
        name_list = name.split('_')
        standarized_name_list = [i.capitalize() for i in name_list]
        standarized_name = '_'.join([w for w in standarized_name_list])
        return standarized_name

    def create_intent(self,name):
        print("Creating intent")
        intent_node=Node('intent')
        intent_node['name']=self.word_normalizer(name)
        self.graph.merge(intent_node,'intent','name')

    def create_context(self,name):
        print("creating context")
        context_node = Node('context')
        context_node['name'] = self.word_normalizer(name)
        self.graph.merge(context_node, 'context', 'name')

    #node that creates the utterance or updates if already present
    def create_utterance(self,name):
        print("creating utterance")
        utter_node = Node('utter')
        utter_node['name'] = self.word_normalizer(name)
        self.graph.merge(utter_node, 'utter', 'name')

    #code that create the actions if not present or updates the action if any already present
    def create_actions(self,name):
        print("creating action")
        action_node=Node('action')
        action_node['name'] = self.word_normalizer(name)
        self.graph.merge(action_node,'action','name')

    def create_relation(self, context, relation_tracker):

        for each_relation, idx in relation_tracker:
            node_a = Node(context[idx - 1][0])
            node_a['name'] = self.word_normalizer(context[idx - 1][1])
            node_b = Node(context[idx + 1][0])
            node_b['name'] = self.word_normalizer(context[idx + 1][1])
            relation_type = each_relation['type']
            relation = Relationship(node_a, relation_type , node_b)
            for attr,attr_value in each_relation.items():
               
                relation[attr] = attr_value

            self.graph.merge(node_a, context[idx - 1][0], 'name')
            self.graph.merge(node_b, context[idx + 1][0], 'name')
            self.graph.merge(relation)

        print("<===========<<<RELATION---CREATION---COMPLETE>>>===============>")



class CreateResponseWireframe:

    def __init__(self):
        self.graph = Graph(
                            'bolt://localhost:7687',
                            auth=('neo4j', 'subodh')
                           )


    def get_graph_utterances(self):
        utterances_list=[]
        results=self.graph.run(
            '''MATCH (u:utter) return  u'''
        )

        for utterance in results:
            value=list(utterance.values())
            utterances_list.extend([t for t in value[0].values()])
        return utterances_list

    def get_graph_actions(self):
        action_list=[]
        results=self.graph.run('''MATCH (a:action) return a''')
        for action in results:
            value=list(action.values())
            action_list.extend([t for t in value[0].values()])
            print(action_list)
        return action_list

    def get_graph_context(self):
        context_list = []
        results = self.graph.run('''MATCH (c:context) return c''')
        for action in results:
            value = list(action.values())
            context_list.extend([t for t in value[0].values()])
        return context_list

                  


if __name__=='__main__':

    graphdb_obj=CreateGraphKB()
    wireframe_obj=CreateResponseWireframe()
    choices = {
        'intent': graphdb_obj.create_intent,
        'action':graphdb_obj.create_actions,
        'utter':graphdb_obj.create_utterance,
        'context':graphdb_obj.create_context
    }
    for each_dialouge in dialouges:
        context = []
        relation_tracker = []
        idx = 0
        for each_component in each_dialouge:

            for key,value in each_component.items():
                print ("eachhhhh",value)
                try:
                    choices[key](value)
                    context.append((key,value))
                    idx += 1

                except:
                    relation_tracker.append((value,idx))
                    print("RrrrR",relation_tracker)
                    context.append(idx)
                    idx += 1



        graphdb_obj.create_relation(context,relation_tracker)

    utterances,actions,context=wireframe_obj.get_graph_utterances(),\
                               wireframe_obj.get_graph_actions(),\
                               wireframe_obj.get_graph_context()

    with open(os.getcwd() + '/../actions/utterance.json') as file:
        content = json.load(file)
        content = content
    for utterance in utterances:
        try:
            if content[utterance]:
                pass
        except:
            content.update({utterance:[]})
    with open(os.getcwd() + '/../actions/utterance.json','w') as file:
        json.dump(content,file)










