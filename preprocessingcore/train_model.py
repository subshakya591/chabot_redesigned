import os
import pickle
import json
import tensorflow as tf
import tflearn
from nltk.tokenize import word_tokenize
from data_ready import Prepare_data

class TensorflowModel:

    def __init__(self,train_x,train_y):
        self.working_directory=os.getcwd()
        self.train_x,self.train_y=train_x,train_y

    def create_neural_net(self):
        tf.reset_default_graph()
        # Build neural network
        input_layer = tflearn.input_data(shape=[None, len(self.train_x[0])])
        hidden_layer1 = tflearn.fully_connected(input_layer, 8)
        hidden_layer2 = tflearn.fully_connected(hidden_layer1, 8)
        output_layer = tflearn.fully_connected(hidden_layer2, len(self.train_y[0]), activation='softmax')
        self.regression_model = tflearn.regression(output_layer)
        return ("<-----@neural_net_successfully_defined_with_two_hidden_layers------> ")

    def create_model(self):
        model = tflearn.DNN(self.regression_model, tensorboard_dir='tflearn_logs',tensorboard_verbose=3)
        model.fit(self.train_x, self.train_y, n_epoch=1000, batch_size=8, show_metric=True)
        model.save(self.working_directory+'/R_models/intent_classifier.tflearn')
        return("<-----@model_run_and_trained_sucessfully------> ")

    def pickle_ds(self,tokens,labels,train_x,train_y):
        print("<-----@data_being_pickled-------->")
        pickle.dump({'tokens': tokens, 'labels': labels, 'train_x': train_x, 'train_y': train_y},open(self.working_directory+"/../pickles/t_metadata", "wb"))
        return("<-----@pickling_complete--------->")


class StanfordEntityTrainer:

    def __init__(self):
        with open (os.getcwd()+'/../dataset/training_set.json') as jsonfile:
            self.contents=json.load(jsonfile)

    def prepare_for_entity_extraction(self):
        entity_list = []
        dumping_dictionary = {}

        index = ['first_entity','second_entity','third_entity']
        keys =[]
        for key_,value_ in self.contents.items():
            queries=value_.get('query')
            entities = value_.get('entity')
            entity_index = {}
            for entity_dict in entities:
                i = 0

                for k,v in entity_dict.items():
                    if v not in entity_list:
                        entity_list.append(v)
                        entity_index.update({v:index[i]})
                        i+=1
                if key_ not in keys:
                    keys.append(key_)
                    dumping_dictionary.update({key_:entity_index})

                else:
                    pass
                entity_index = {}










            with open (os.getcwd()+'/../nerfiles/train/train.tsv','a') as file:
                for query,entity in zip(queries,entities):

                    tokens=word_tokenize(query)
                    for each_token in tokens:
                        try:
                            final_word=each_token + ' ' + entity[each_token]
                            file.write(final_word + "\n")
                        except:
                            final_word=each_token + ' ' + 'O'
                            file.write(final_word + "\n")
                    file.write("\n"+"\n")

            with open(os.getcwd() + '/../knowledgetracker/entity_indexer.json', 'r') as jsonfile:

                contents = json.load(jsonfile)
                print(contents)

                print("<-------NER data Preparation Complete--------------->")
        print("dumping file",dumping_dictionary)

        with open(os.getcwd() + '/../knowledgetracker/entity_indexer.json', 'w') as jsonfile:
            json.dump(dumping_dictionary,jsonfile)




    def train_ner_model(self):
        os.system('''cd ../nerfiles/
                    java -cp "stanford-ner.jar:lib/*" -mx4g edu.stanford.nlp.ie.crf.CRFClassifier -prop train/prop.txt'''
                  )
        print("<----------Training Process Completed-------------->")


if __name__=='__main__':
    a = Prepare_data()
    print(a.read_file())
    print(a.preprocess_data())
    print(a.format_data())
    print(a.add_training_set())
    print(a.prepare_training_set())
    b=TensorflowModel(a.train_x,a.train_y)
    print(b.create_neural_net())
    print(b.create_model())
    print(b.pickle_ds(a.tokens,a.labels,a.train_x,a.train_y))
    print("<==========NEURAL NET TRAINED FOR INTENT RECOGNITION==========>")
    k = StanfordEntityTrainer()
    k.prepare_for_entity_extraction()
    k.train_ner_model()


