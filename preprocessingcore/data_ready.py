import os
import numpy as np
import json
import nltk
from nltk.stem.lancaster import LancasterStemmer
import random


class Prepare_data:
    def __init__(self):
        self.tokens=[]
        self.labels=[]
        self.doc=[]
        self.stop_words=['the,is,?']
        self.training_set=[]
        self.result=[]
        self.stemmer=LancasterStemmer()
        self.current_directory=os.getcwd()
    def read_file(self):
        with open (self.current_directory+'/../dataset/training_set.json') as jsonFile:
            self.intents=json.load(jsonFile)
        return ("<-----@file_loaded----->")

    def tokenize_words(self,sentence,stem_flag):
        if stem_flag==False:
            return (nltk.word_tokenize(sentence))
        elif stem_flag==True:
            tokens=nltk.word_tokenize(sentence)
            return ([self.stemmer.stem(word.lower())for word in tokens])

    def preprocess_data(self):
        for intent in self.intents.items():
            for qst in intent[1]['query']:
                #tokenize each word in the sentence
                w_tokens=self.tokenize_words(qst,False)
                #merge two list with extent
                self.tokens.extend(w_tokens)
                self.doc.append((w_tokens,intent[0]))
                if intent[0] not in self.labels:
                    self.labels.append(intent[0])
        self.result_zeros = [0] * len(self.labels)
        return ("<-----@data_preprocessing_complete---->")

    def format_data(self):
        self.tokens=[self.stemmer.stem(word.lower()) for word in self.tokens if word not in self.stop_words]
        self.tokens=sorted(list(set(self.tokens)))
        self.labels=sorted(list(set(self.labels)))
        return ("<-----@Data_Normalization_complete----->")

    def add_training_set(self):
        for elements in self.doc:
            cbow=[]
            query_words=elements[0]
            query_words=[self.stemmer.stem(word.lower()) for word in query_words]
            for w in self.tokens:
                cbow.append(1) if w in query_words else cbow.append(0)
            result_out = list(self.result_zeros)
            #print("iam result out",result_out)
            result_out[self.labels.index(elements[1])] = 1
            self.training_set.append([cbow,result_out])
        return ("<-----@added_training_set----->")

    def prepare_training_set(self):
        random.shuffle(self.training_set)
        self.training_set=np.array(self.training_set)
        self.train_x=list(self.training_set[:,0])
        self.train_y=list(self.training_set[:,1])
        return("<-----@prepared_training_data----->")


