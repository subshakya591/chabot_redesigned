import os
import random
import requests
import json


class Utterance:

    def __init__(self):
        with open(os.getcwd()+'/actions/utterance.json') as file:
            self.content = json.load(file)

    @classmethod
    def get_classname(cls):
        return cls.__name__

    def run(self,intent):
        print(intent)
        reply = random.choice(self.content[intent])
        return (reply)



class Context:

    def __init__(self):

        with open(os.getcwd()+'/actions/context.json') as file:
            # import pdb;
            # pdb.set_trace()
            self.content=json.load(file)

    @classmethod
    def get_classname(cls):
        return cls.__name__

    def run(self,context,context_mode):
        # import pdb;pdb.set_trace()
        reply_pool = self.content[context]
        reply = reply_pool[context_mode]
        return reply



class Actions:
    def __init__(self):
        self.headers = {'content-type': 'application/json'}

    @classmethod
    def get_classname(cls):
        return cls.__name__


class ActionAssign_Task(Actions):

    def run(self,entity_tracker):
      print(entity_tracker)
      return ("how are you ggggg")

class ActionInvite_Cleaner(Actions):

    def run(self,entity_tracker):
        print(entity_tracker)
        return ("Cleaner Has been invited")


#'Show_Listing', 'Show_Taskmaster', 'Show_Reviews', 'Invite_Cleaner', 'List_Status

class ActionShow_Listing(Actions):

    def run(self,entity_tracker):
        import pdb;pdb.set_trace()
        print(entity_tracker)
        return("sucessfully called show_listings")

class ActionShow_Taskmaster(Actions):

    def run(self,entity_tracker):
        print(entity_tracker)
        return("sucessfully called Taskmaster")

class ActionList_status(Actions):

    def run(self,entity_tracker):
        print(entity_tracker)
        return("sucessfully called liststatus")

class ActionShow_Reviews(Actions):

    def run(self,entity_tracker):

        print("sucessfully called Show Reviews")
        return ('sucessfully')








