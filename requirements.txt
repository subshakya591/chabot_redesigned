Django==2.2.2
djangorestframework==3.9.4
djangorestframework-simplejwt==4.3.0
Flask==1.0.2
neo4j-driver==1.6.2
neo4jrestclient==2.1.1
neobolt==1.7.13
nltk==3.4
numpy==1.15.4
py2neo==4.1.3
PyJWT==1.7.1
tensorflow==1.13.1


